// Zandronum Competitive Configurator - Doom Modification for GZDoom
// Copyright (C) 2024  William Weber Berrutti (aka Cyantusk)

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// White
Actor HealthBonus_Stacked_Death : HealthBonus
{
    Inventory.Amount 5
    Translation "192:207=80:103", "240:247=104:111", "6:6=110:110", "0:0=111:111", "6:6=109:109"
    Inventory.PickupMessage "Picked up a +5 health bonus"

    States
    {
    Spawn:
        TNT1 A 0 NoDelay ACS_NamedExecuteAlways("FragDropPickupDespawnScript_ZCC")
    SpawnContinue:
        BON1 ABCDCB 6 Light("HealthBonus_Stacked_Death_Light")
        Loop
    Despawn:
        TNT1 A 0
        Stop
    }
}

Actor ArmorBonus_Stacked_Death : ArmorBonus
{
    Armor.SaveAmount 5
    Translation "112:127=80:111"
    Inventory.PickupMessage "Picked up a +5 armor bonus"

    States
    {
    Spawn:
        TNT1 A 0 NoDelay ACS_NamedExecuteAlways("FragDropPickupDespawnScript_ZCC")
    SpawnContinue:
        BON2 ABCDCB 6 Light("ArmorBonus_Stacked_Death_Light")
        Loop
    Despawn:
        TNT1 A 0
        Stop
    }
}

// Gold
Actor HealthBonus_Stacked_ExtremeDeath : HealthBonus
{
    Inventory.Amount 10
    Inventory.PickupMessage "Picked up a +10 health bonus"

    States
    {
    Spawn:
        TNT1 A 0 NoDelay ACS_NamedExecuteAlways("FragDropPickupDespawnScript_ZCC")
    SpawnContinue:
        BON5 ABCDCB 6 Light("HealthBonus_Stacked_ExtremeDeath_Light")
        Loop
    Despawn:
        TNT1 A 0
        Stop
    }
}

Actor ArmorBonus_Stacked_ExtremeDeath : ArmorBonus
{
    Armor.SaveAmount 10
    Translation "112:127=160:167"
    Inventory.PickupMessage "Picked up a +10 armor bonus"

    States
    {
    Spawn:
        TNT1 A 0 NoDelay ACS_NamedExecuteAlways("FragDropPickupDespawnScript_ZCC")
    SpawnContinue:
        BON2 ABCDCB 6 Light("ArmorBonus_Stacked_ExtremeDeath_Light")
        Loop
    Despawn:
        TNT1 A 0
        Stop
    }
}