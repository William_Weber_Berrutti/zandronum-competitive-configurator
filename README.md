# Zandronum Competitive Configurator

## Description

It's an experimental mod that brings some competitive features to Zandronum inspired by Quake 3's competitive mods and Unreal Tournament's mutators.

**IMPORTANT:** This mod requires Doom 2 with Skulltag Content (on Zandronum) to work!

At the moment, the features are:

- No Random Damage: all weapons do constant damage. Pistol, Super Shotgun, Shotgun, Minigun and Chaingun have custom firing patterns. Due do Zandronum limitations, the BFG was changed to have a big splash damage instead of tracers
- Gamemode Compatibility: It should be compatible with all Zandronum gamemodes, and probably custom ones
- Convenience: server hosts can change the custom CVAR values during the match, not needing to restart the map
- Flexibility: the new CVARs can be enabled or disabled at will and are independent of each other


### CVars

**NOTE:** All CVARs are dynamic, meaning they all take effect after changing them.

**ZCC_WeaponArena <-1..10\>:** Enables only a specific weapon for the match.

Avaliable values are:

	-1 (or any other value): Enables all weapons like normal
	0: Chainsaw
	1: Fist (with Berserk)
	2: Pistol
	3: Super Shotgun
	4: Shotgun
	5: Minigun
	6: Chaingun
	7: Grenade Launcher
	8: Rocket Launcher
	9: Railgun
	10: Plasma Rifle



- **ZCC_NoBFG <1|0\>:** Enables/Disables BFGs in the entire map.

- **ZCC_RespawnProtection <1|0\>:** Gives time proportional respawn protection to (re)spawning players. "SV_NoRespawnInvul" should be enabled.

- **ZCC_FragDrops <1|0\>:** Players drop a small amount of health and armor after fragged. Better items are spawned when players are gibbed.


## Licensing

Zandronum Competitive Configurator project is licensed under the [GPL-3](http://www.gnu.org/licenses/gpl.html).

Recolored graphics (by Cyantusk) from Doom (by id Software) are not covered in this license.
