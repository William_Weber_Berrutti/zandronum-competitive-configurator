// Zandronum Competitive Configurator - Doom Modification for GZDoom
// Copyright (C) 2024  William Weber Berrutti (aka Cyantusk)

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#library "ZCC"
#include "zcommon.acs"

#define WEAPON_ARENA_NONE -1
#define WEAPON_ARENA_CHAINSAW 0
#define WEAPON_ARENA_FIST 1
#define WEAPON_ARENA_PISTOL 2
#define WEAPON_ARENA_SUPERSHOTGUN 3
#define WEAPON_ARENA_SHOTGUN 4
#define WEAPON_ARENA_MINIGUN 5
#define WEAPON_ARENA_CHAINGUN 6
#define WEAPON_ARENA_GRENADELAUNCHER 7
#define WEAPON_ARENA_ROCKETLAUNCHER 8
#define WEAPON_ARENA_RAILGUN 9
#define WEAPON_ARENA_PLASMARIFLE 10

#define WEAPON_ARENA_AMOUNT 11
#define WEAPONS_AMOUNT 13
#define AMMO_AMOUNT 4   // Ammo types amount
#define AMMO_SIZES 2   // Small and Big ammo items

#define RESPAWN_PROTECTION_DELAY 30   // ~1 second

#define BIG_INT 0x7FFFFFFF


int AvailableWeaponArenaWeapons[WEAPONS_AMOUNT] =
{ "Chainsaw_ZCC", "Fist_ZCC", "Pistol_ZCC", "SuperShotgun_ZCC", "Shotgun_ZCC", "Minigun_ZCC", "Chaingun_ZCC", "GrenadeLauncher_ZCC", "RocketLauncher_ZCC", "Railgun_ZCC", "PlasmaRifle_ZCC", "BFG10K_ZCC", "BFG9000_ZCC" };

int AvailableWeaponArenaAmmoTypes[WEAPON_ARENA_AMOUNT] =
{ "None", "None", "Clip", "Shell", "Shell", "Clip", "Clip", "RocketAmmo", "RocketAmmo", "Cell", "Cell" };

int AvailableWeaponArenaBgAmmoTypes[WEAPON_ARENA_AMOUNT] =
{ "None", "None", "ClipBox", "ShellBox", "ShellBox", "ClipBox", "ClipBox", "RocketBox", "RocketBox", "CellPack", "CellPack" };

int AvailableWeaponArenaAmmoAmount[WEAPON_ARENA_AMOUNT] =
{ 0, 0, 50, 24, 24, 100, 100, 15, 15, 150, 100 };

int AmmoDropAmounts[WEAPON_ARENA_AMOUNT] =
{ 0, 0, 50, 20, 20, 100, 100, 10, 10, 100, 100 };

int LMSAmmoAmounts[WEAPON_ARENA_AMOUNT] =
{ 0, 0, 400, 100, 100, 400, 400, 100, 100, 600, 600 };

int WeaponArenaAmmoTypes[AMMO_SIZES][WEAPON_ARENA_AMOUNT] =
{ { "None", "None", "Clip_ZCC", "Shell_ZCC", "Shell_ZCC", "Clip_ZCC", "Clip_ZCC", "RocketAmmo_ZCC", "RocketAmmo_ZCC", "Cell_ZCC", "Cell_ZCC" }, { "None", "None", "ClipBox_ZCC", "ShellBox_ZCC", "ShellBox_ZCC", "ClipBox_ZCC", "ClipBox_ZCC", "RocketBox_ZCC", "RocketBox_ZCC", "CellPack_ZCC", "CellPack_ZCC" } };

int AmmoTypes[AMMO_SIZES][AMMO_AMOUNT] =
{ { "Clip_ZCC", "Shell_ZCC", "RocketAmmo_ZCC", "Cell_ZCC" }, { "ClipBox_ZCC", "ShellBox_ZCC", "RocketBox_ZCC", "CellPack_ZCC" }};

/**
* == Description ==
* Defines the basic functionalities
**/
Script "StartingScript_ZCC" ENTER
{
    if (GetCVar("ZCC_RespawnProtection"))
    {
        ACS_NamedExecuteAlways("RespawnProtection", 0);
    }

    int weaponArenaValueOld = 99999;
    int weaponArenaValueNew = 0;

    while (!CheckFlag(0, "CORPSE"))
    {
        weaponArenaValueNew = CheckDefaultWeaponArenaValue();

        if (weaponArenaValueNew != weaponArenaValueOld)
        {
            ACS_NamedExecuteAlways("WeaponArenaScript_ZCC", 0);
            weaponArenaValueOld = weaponArenaValueNew;
        }

        ACS_NamedExecuteAlways("WeaponArenaAmmoDropScript_ZCC", 0);

        ACS_NamedExecuteAlways("NoBFGManagerScript_ZCC", 0);

        ACS_NamedExecuteAlways("FragDropsScript_ZCC", 0);

        Delay(1);
    }
}

/**
* == Description ==
* Aids on player respawn situations
**/
Script "PlayerRespawn_ZCC" RESPAWN
{
    ACS_NamedExecuteAlways("StartingScript_ZCC", 0, 0, 0, 0);
}

/**
* == Description ==
* Aids on player death situations
**/
Script "PlayerDeath" DEATH
{
    SetActorProperty(0, APROP_RenderStyle, STYLE_Normal);
}

/**
* == Description ==
* Determines which ammo to spawn
* 
* == Parameters ==
* ammoSize: 0 - Small ammo boxes, 1 - Big ammo boxes
* originalAmmo: 0 - Clip, 1 - Shell, 2 - Rocket, 3 - Cell
**/
Script "GenericAmmoSpawnScript_ZCC" (int ammoSize, int originalAmmo)
{
    int prevWeaponArenaValue = 'a';

    while(1)
    {
        int currentWeaponArenaValue = CheckDefaultWeaponArenaValue();
        int currentAmmoType;

        if (currentWeaponArenaValue != prevWeaponArenaValue)
        {
            if (currentWeaponArenaValue >= WEAPON_ARENA_PISTOL && currentWeaponArenaValue <= WEAPON_ARENA_PLASMARIFLE)
            {
                currentAmmoType = WeaponArenaAmmoTypes[ammoSize][currentWeaponArenaValue];
            }
            else if (currentWeaponArenaValue < WEAPON_ARENA_CHAINSAW || currentWeaponArenaValue > WEAPON_ARENA_PLASMARIFLE)
            {
                currentAmmoType = AmmoTypes[ammoSize][originalAmmo];
            }
            else
            {
                currentAmmoType = "None";
            }

            int x = GetActorX(0);
            int y = GetActorY(0);
            int z = GetActorZ(0);

            SpawnForced(currentAmmoType, x, y, z);

            prevWeaponArenaValue = currentWeaponArenaValue;
        }

        Delay(1);
    }
}

/**
* == Description ==
* Determines which weapon to spawn
* 
* == Parameters ==
* originalAmmo: 0 - Clip, 1 - Shell, 2 - Rocket, 3 - Cell
**/
Script "GenericWeaponSpawnScript_ZCC" (int originalWeapon)
{
    int prevWeaponArenaValue = 'a';

    while(1)
    {
        int currentWeaponArenaValue = CheckDefaultWeaponArenaValue();
        int currentWeapon;

        if (currentWeaponArenaValue != prevWeaponArenaValue)
        {
            if (currentWeaponArenaValue >= WEAPON_ARENA_PISTOL && currentWeaponArenaValue <= WEAPON_ARENA_PLASMARIFLE)
            {
                currentWeapon = AvailableWeaponArenaWeapons[currentWeaponArenaValue];
            }
            else if (currentWeaponArenaValue < WEAPON_ARENA_CHAINSAW || currentWeaponArenaValue > WEAPON_ARENA_PLASMARIFLE)
            {
                currentWeapon = AvailableWeaponArenaWeapons[originalWeapon];
            }
            else
            {
                currentWeapon = "None";
            }

            int x = GetActorX(0);
            int y = GetActorY(0);
            int z = GetActorZ(0);

            SpawnForced(currentWeapon, x, y, z);

            prevWeaponArenaValue = currentWeaponArenaValue;
        }

        Delay(1);
    }
}

/**
* == Description ==
* Determines when to despawn ammo or weapon pickups
**/
Script "AmmoPickupDespawnScript_ZCC" (void)
{
    int prevWeaponArenaValue = CheckDefaultWeaponArenaValue();

    while (1)
    {
        int currentWeaponArenaValue = CheckDefaultWeaponArenaValue();

        if (!CheckFlag(0, "DROPPED") && currentWeaponArenaValue >= WEAPON_ARENA_CHAINSAW && currentWeaponArenaValue <= WEAPON_ARENA_PLASMARIFLE || currentWeaponArenaValue != prevWeaponArenaValue)
        {
            SetActorState(0, "Despawn");
            terminate;
        }

        Delay(1);
    }
}

/**
* == Description ==
* Determines when to despawn ammo or weapon pickups
**/
Script "WeaponPickupDespawnScript_ZCC" (int isBFG)
{
    int prevWeaponArenaValue = CheckDefaultWeaponArenaValue();

    while (1)
    {
        int currentWeaponArenaValue = CheckDefaultWeaponArenaValue();
        int noBFG = GetCVar("ZCC_NoBFG");

        if (isBFG == 0 && !CheckFlag(0, "DROPPED") && currentWeaponArenaValue >= WEAPON_ARENA_CHAINSAW && currentWeaponArenaValue <= WEAPON_ARENA_PLASMARIFLE || prevWeaponArenaValue != currentWeaponArenaValue)
        {
            SetActorState(0, "Despawn");
            terminate;
        }

        if ((isBFG == 1 && noBFG == 1) || !CheckFlag(0, "DROPPED") && (currentWeaponArenaValue >= WEAPON_ARENA_CHAINSAW && currentWeaponArenaValue <= WEAPON_ARENA_PLASMARIFLE || prevWeaponArenaValue != currentWeaponArenaValue))
        {
            SetActorState(0, "Despawn");
            terminate;
        }

        Delay(1);
    }
}

/**
* == Description ==
* Determines when to despawn ammo or weapon pickups
**/
Script "PickupSpawnScript_ZCC" (int isBFG)
{
    while (1)
    {
        int currentWeaponArenaValue = CheckDefaultWeaponArenaValue();
        int noBFG = GetCVar("ZCC_NoBFG");

        if (isBFG == 0 && !CheckFlag(0, "DROPPED") && currentWeaponArenaValue < WEAPON_ARENA_CHAINSAW || currentWeaponArenaValue > WEAPON_ARENA_PLASMARIFLE)
        {
            SetActorState(0, "Spawn");
            terminate;
        }

        if ((isBFG == 1 && noBFG == 0) && !CheckFlag(0, "DROPPED") && (currentWeaponArenaValue < WEAPON_ARENA_CHAINSAW || currentWeaponArenaValue > WEAPON_ARENA_PLASMARIFLE))
        {
            SetActorState(0, "Spawn");
            terminate;
        }

        Delay(1);
    }
}

/**
* == Description ==
* Defines weapon and ammo pickups for players according to 'ZCC_WeaponArena' value
**/
Script "WeaponArenaScript_ZCC" ENTER
{
    // For some reason, if I use 'ClearInventory()' here it plays the Chainsaw sound on LMS and TeamLMS gamemodes!
    ClearInventory_ZCC();

    int lastManStanding  = GetCVar("LastManStanding") || GetCVar("TeamLMS");

    if (lastManStanding)
    {
        SetActorProperty(0, APROP_Health, 200);
        GiveInventory("GreenArmor", 1);
    }
    else if (CheckInventory("PowerTerminatorArtifact"))
    {
        SetActorProperty(0, APROP_Health, 200);
        GiveInventory("BlueArmor", 1);
    }
    else
    {
        SetActorProperty(0, APROP_Health, 100);
    }

    int weaponArenaValue = CheckDefaultWeaponArenaValue();

    switch (weaponArenaValue)
    {
        default:
        case WEAPON_ARENA_NONE:
            if (lastManStanding)
            {
                SetAmmoCapacity("Clip", 200);
                SetAmmoCapacity("Shell", 50);
                SetAmmoCapacity("RocketAmmo", 50);
                SetAmmoCapacity("Cell", 300);

                for (int i = 0; i < WEAPON_ARENA_AMOUNT; i++)
                {
                    GiveInventory(AvailableWeaponArenaWeapons[i], 1);
                    GiveInventory(AvailableWeaponArenaBgAmmoTypes[i], LMSAmmoAmounts[i]);
                }

                SetWeapon("PlasmaRifle_ZCC");
            }
            else
            {
                GiveInventory("Pistol_ZCC", 1);
                GiveInventory("Fist_ZCC", 1);
                SetWeapon("Pistol_ZCC");
            }
            break;
        case WEAPON_ARENA_CHAINSAW:
            GiveInventory(AvailableWeaponArenaWeapons[weaponArenaValue], 1);
            SetWeapon(AvailableWeaponArenaWeapons[weaponArenaValue]);
            break;
        case WEAPON_ARENA_FIST:
            GiveInventory(AvailableWeaponArenaWeapons[weaponArenaValue], 1);
            SetWeapon(AvailableWeaponArenaWeapons[weaponArenaValue]);
            GiveInventory("Berserk", 1);
            break;
        case WEAPON_ARENA_PISTOL:
        case WEAPON_ARENA_SHOTGUN:
        case WEAPON_ARENA_SUPERSHOTGUN:
        case WEAPON_ARENA_MINIGUN:
        case WEAPON_ARENA_CHAINGUN:
        case WEAPON_ARENA_GRENADELAUNCHER:
        case WEAPON_ARENA_ROCKETLAUNCHER:
        case WEAPON_ARENA_RAILGUN:
        case WEAPON_ARENA_PLASMARIFLE:
            GiveInventory(AvailableWeaponArenaWeapons[weaponArenaValue], 1);
            TakeInventory(AvailableWeaponArenaAmmoTypes[weaponArenaValue], BIG_INT);

            if (lastManStanding)
            {
                SetAmmoCapacity("Clip", 400);
                SetAmmoCapacity("Shell", 100);
                SetAmmoCapacity("RocketAmmo", 100);
                SetAmmoCapacity("Cell", 600);

                GiveInventory(AvailableWeaponArenaBgAmmoTypes[weaponArenaValue], LMSAmmoAmounts[weaponArenaValue]);
            }
            else
            {
                GiveInventory(AvailableWeaponArenaAmmoTypes[weaponArenaValue], AvailableWeaponArenaAmmoAmount[weaponArenaValue]);
            }

            SetWeapon(AvailableWeaponArenaWeapons[weaponArenaValue]);
            break;
    }
}

/**
* == Description ==
* Manages players inventory when using ZCC_NoBFG CVar
**/
Script "NoBFGManagerScript_ZCC" (void)
{
    int noBFG = GetCVar("ZCC_NoBFG");

    if (noBFG)
    {
        TakeInventory("BFG9000_ZCC", 1);
        TakeInventory("BFG10K_ZCC", 1);
    }
}

/**
* == Description ==
* Gives player resistance after (re)spawning, according to ZCC_RespawnProtection CVar
**/
Script "RespawnProtection" (void)
{
    if (CheckFlag(0, "CORPSE"))
    {
        terminate;
    }

    // Needs to be this low because of direct BFGBall shots, and needs to be > 0 so it doesn't make players stuck on a spawn point by preventing spawning telefrags!
    SetActorProperty(0, APROP_DamageFactor, 0.125);
    SetActorProperty(0, APROP_RenderStyle, STYLE_Add);

    // So players that died during the resistance period do not mistakenly receive wrong damagefactors (if they respawn fast enough)
    int i = 0;
    while (i < RESPAWN_PROTECTION_DELAY/(CheckDefaultWeaponArenaValue() > WEAPON_ARENA_NONE ? 2 : 1))
    {
        if (CheckFlag(0, "CORPSE"))
        {
            terminate;
        }

        Delay(1);
        i++;
    }

    SetActorProperty(0, APROP_DamageFactor, 1.0);
    SetActorProperty(0, APROP_RenderStyle, STYLE_Normal);
}

/**
* == Description ==
* Drops ammo items after fragging players (for when 'ZCC_WeaponArena' is active)
**/
Script "WeaponArenaAmmoDropScript_ZCC" (void)
{
    int currentWeapon = GetWeapon();
    int weaponArenaValue = CheckDefaultWeaponArenaValue();

    if (weaponArenaValue > WEAPON_ARENA_NONE && weaponArenaValue < WEAPON_ARENA_AMOUNT)
    {
        if (CheckFlag(0, "CORPSE"))
        {
            DropItem(0, AvailableWeaponArenaBgAmmoTypes[weaponArenaValue], AmmoDropAmounts[weaponArenaValue]);
        }
    }
}

/**
* == Description ==
* Drops items after fragging players
**/
Script "FragDropsScript_ZCC" (void)
{
    int fragDrops = GetCVar("ZCC_FragDrops");

    if (fragDrops)
    {
        if (CheckFlag(0, "CORPSE"))
        {
            if (GetActorProperty(0, APROP_Health) >= -100)
            {
                DropItem(0, "HealthBonus_Stacked_Death", 5);
                DropItem(0, "ArmorBonus_Stacked_Death", 1);
            }
            else
            {
                DropItem(0, "HealthBonus_Stacked_ExtremeDeath", 10);
                DropItem(0, "ArmorBonus_Stacked_ExtremeDeath", 1);
            }
            
        }
    }
}

/**
* == Description ==
* Despawn frag drop items if 'ZCC_FragDrops' is disabled or 'ZCC_WeaponArena' is changed
**/
Script "FragDropPickupDespawnScript_ZCC" (void)
{
    int prevFragDropsValue = GetCVar("ZCC_FragDrops");
    int prevWeaponArenaValue = CheckDefaultWeaponArenaValue();

    while (1)
    {
        int currentFragDropsValue = GetCVar("ZCC_FragDrops");
        int currentWeaponArenaValue = CheckDefaultWeaponArenaValue();

        if (currentFragDropsValue != prevFragDropsValue || currentWeaponArenaValue != prevWeaponArenaValue)
        {
            SetActorState(0, "Despawn");
            terminate;
        }

        Delay(1);
    }
}

/**
* == Description ==
* Clears players inventory. NOTE: It doesn't remove powerups since that might be annoying for players!
**/
function void ClearInventory_ZCC(void)
{
    for (int i = 0; i < WEAPONS_AMOUNT; i++)
    {
        TakeInventory(AvailableWeaponArenaWeapons[i], 1);
        TakeInventory(AvailableWeaponArenaAmmoTypes[i], BIG_INT);
    }

    TakeInventory("BackPack", 1);
    TakeInventory("Armor", BIG_INT);
}

/**
* == Description ==
* Checks and forces a default value (if necessary) for 'ZCC_WeaponArena' when it's above WEAPON_ARENA_PLASMARIFLE
**/
function int CheckDefaultWeaponArenaValue(void)
{
    if (GetCVar("ZCC_WeaponArena") > WEAPON_ARENA_PLASMARIFLE)
    {
        SetCVar("ZCC_WeaponArena", WEAPON_ARENA_NONE);
    }

    return GetCVar("ZCC_WeaponArena");
}